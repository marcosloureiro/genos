package LeadsControl;

import java.util.Date;

public class Usuario {
	private int id;
	private String nome;
	private String email;
	private String celular;
	private Date data_tadastro;
	private int patrocinador;
	private int tipo;
	public static String[] tipoArgs = {"leads", "usuario", "patrocinador"};
	
	public Usuario(int tipo) {
		this.tipo = tipo;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public Date getData_tadastro() {
		return data_tadastro;
	}
	public void setData_tadastro(Date data_tadastro) {
		this.data_tadastro = data_tadastro;
	}
	public int getPatrocinador() {
		return patrocinador;
	}
	public void setPatrocinador(int patrocinador) {
		this.patrocinador = patrocinador;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	
	
	
}
